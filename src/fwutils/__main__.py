import argparse
from io import BytesIO
import mmap
import pathlib
import zlib
from fwutils.definitions.spa504g import Spa504g


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Firmware utility for the SPA504G SIP phone.")
    subparsers = parser.add_subparsers(help="Extract a file.")
    
    extract_parser = subparsers.add_parser("x")
    extract_parser.add_argument("file", type=pathlib.Path, nargs="?", help="The file to extract.")

    args = parser.parse_args()
    if not args.file:
        parser.error("You must specify a file.")

    return args


def main():
    args = parse_args()
    file: pathlib.Path = args.file

    file_handle = open(file, "rb")
    with mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ) as buf:
        firmware = Spa504g.from_io(BytesIO(buf))

        output_dir = file.parent.absolute() / "extracted"
        output_dir.mkdir(parents=True, exist_ok=True)
        for module in firmware.modules:
            output_file = file.stem 
            output_file += "_" 
            output_file += hex(module.offset) 
            output_file += "_" 
            output_file += hex(module.offset + module.length)
            output_file += file.suffix
            output_file = pathlib.Path(output_file)

            with open(output_dir / output_file, "wb") as f:
                f.write(module.body)

            if module.compressed == 0: # 0 == True
                decompressed_data = zlib.decompress(module.body, 15)
                output_file = output_file.stem + "_decompressed" + output_file.suffix
                with open(output_dir / output_file, "wb") as f:
                    f.write(decompressed_data)
        
    f.close()

if __name__ == "__main__":
    main()
