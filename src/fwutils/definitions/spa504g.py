# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if parse_version(kaitaistruct.__version__) < parse_version('0.9'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Spa504g(KaitaiStruct):
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.header = Spa504g.Header(self._io, self, self._root)
        self.modules = [None] * (self.header.module_count)
        for i in range(self.header.module_count):
            self.modules[i] = Spa504g.Module(self._io, self, self._root)


    class Header(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.magic = self._io.read_bytes(16)
            if not self.magic == b"\x53\x6B\x4F\x73\x4D\x6F\x35\x20\x66\x49\x72\x4D\x77\x41\x72\x45":
                raise kaitaistruct.ValidationNotEqualError(b"\x53\x6B\x4F\x73\x4D\x6F\x35\x20\x66\x49\x72\x4D\x77\x41\x72\x45", self.magic, self._io, u"/types/header/seq/0")
            self.signature = self._io.read_bytes(32)
            self.digest = self._io.read_bytes(16)
            self.random_sequence = self._io.read_bytes(16)
            self.header_length = self._io.read_u4be()
            self.module_header_length = self._io.read_u4be()
            self.file_length = self._io.read_u4be()
            self.version = (self._io.read_bytes(32)).decode(u"utf8")
            self.module_count = self._io.read_u4be()
            self.padding = self._io.read_bytes((self.header_length - 128))


    class Module(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sector_id = self._io.read_u2be()
            self.compressed = self._io.read_u2be()
            self.length = self._io.read_u4be()
            self.offset = self._io.read_u4be()
            self.digest = self._io.read_bytes(16)
            self.padding = self._io.read_bytes((self._parent.header.module_header_length - 28))

        @property
        def body(self):
            if hasattr(self, '_m_body'):
                return self._m_body if hasattr(self, '_m_body') else None

            _pos = self._io.pos()
            self._io.seek(self.offset)
            self._m_body = self._io.read_bytes(self.length)
            self._io.seek(_pos)
            return self._m_body if hasattr(self, '_m_body') else None
