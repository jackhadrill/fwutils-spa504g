#!/usr/bin/env python3

from setuptools import setup, find_packages

with open("requirements.txt") as f:
    requirements = f.readlines()

setup(
    name="fwutils",
    version="0.1",
    description="SPA504G firmware utilities.",
    author="Butlersaurus",
    packages=find_packages("src"),
    package_dir={"": "src"},
    include_package_data=True,
    entry_points={"console_scripts": ["fwutils = fwutils.__main__:main"]},
    install_requires=requirements
)
