# SPA504G firmware utilities

Firmware utilities for the Cisco SPA504G SIP phone.

## Installation

This utility requires Python 3.

```bash
$ python3 -m venv venv
$ . ./venv/bin/activate
$ pip install -e .
```

## Usage

The utility will extract files to a new `extracted` directory, in the same location as the specified file.

```
usage: fwutils [-h] {x} ...

Firmware utility for the SPA504G SIP phone.

positional arguments:
  {x}         Extract a file.

options:
  -h, --help  show this help message and exit
```

### Example

```
$ fwutils x ./spa50x-30x-7-6-2f.bin
```
